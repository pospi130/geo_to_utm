﻿using System;

namespace geo_to_utm
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World! - Geo to UTM example.");

            GeoUTMConverter conv = new GeoUTMConverter();

            double lat = 65.03465973823754; // Some latitude
            double lon = 25.464022760355967; // Some longitude

            Console.Write("\nLat: {0} deg\nLon: {1}deg\n", lat, lon);

            conv.ToUTM(lat, lon);

            double x = conv.X;
            double y = conv.Y;
            GeoUTMConverter.Hemisphere hemi = conv.Hemi;
            double zone = conv.Zone;
            
            Console.Write("\nX: {0} m\nY: {1} m\nHemi: {2}\nZone: {3}\n", x, y, hemi, zone);

            conv.ToLatLon(x, y, (int)zone, hemi);

            double new_lat = conv.Latitude;
            double new_lon = conv.Longitude;

            Console.WriteLine("\nResult:");
            Console.WriteLine("-----------------------------------------");
            Console.Write("Lat: {0} deg\nLon: {1} deg\n", new_lat, new_lon);
            Console.WriteLine("Degree difference:");
            Console.Write("Lat: {0:F10} deg\nLon: {1:F10} deg\n", Math.Abs(new_lat - lat), Math.Abs(new_lon - lon));
            Console.WriteLine("-----------------------------------------");

        }
    }
}
